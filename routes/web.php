<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\SalesPersonsController;
use App\Http\Controllers\DashboardController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'product'], function () {
    Route::get('/', [ProductsController::class, 'index'])->name('product');
    Route::get('/list', [ProductsController::class, 'list'])->name('product.list');
    Route::post('/', [ProductsController::class, 'create'])->name('product.create');
    Route::post('/', [ProductsController::class, 'store'])->name('product.store');
    Route::get('/{id}', [ProductsController::class, 'show'])->name('product.show');
    Route::put('/{id}', [ProductsController::class, 'update'])->name('product.update');
    Route::delete('/{id}', [ProductsController::class, 'destroy'])->name('product.destroy');
});

Route::group(['prefix' => 'sales'], function () {
    Route::get('/', [SalesController::class, 'index'])->name('sales');
    Route::post('/', [SalesController::class, 'create'])->name('sales.create');
    Route::post('/', [SalesController::class, 'store'])->name('sales.store');
    Route::get('/{id}', [SalesController::class, 'show'])->name('sales.show');
    Route::put('/{id}', [SalesController::class, 'update'])->name('sales.update');
    Route::delete('/{id}', [SalesController::class, 'destroy'])->name('sales.destroy');
});

Route::group(['prefix' => 'sales-person'], function () {
    Route::get('/', [SalesPersonsController::class, 'index'])->name('sales-person');
    Route::get('/list', [SalesPersonsController::class, 'list'])->name('sales-person-list');
    Route::post('/', [SalesPersonsController::class, 'create'])->name('sales-person.create');
    Route::post('/', [SalesPersonsController::class, 'store'])->name('sales-person.store');
    Route::get('/{id}', [SalesPersonsController::class, 'show'])->name('sales-person.show');
    Route::put('/{id}', [SalesPersonsController::class, 'update'])->name('sales-person.update');
    Route::delete('/{id}', [SalesPersonsController::class, 'destroy'])->name('sales-person.destroy');
});

Route::get('/', [DashboardController::class, 'index']);
Route::group(['prefix' => 'dashboard'], function () {
    Route::get('/sales-per-month', [DashboardController::class, 'salesPerMonth']);
    Route::get('/sales-per-year', [DashboardController::class, 'salesPerYear']);
    Route::get('/sales-per-person', [DashboardController::class, 'salesPerPerson']);
});