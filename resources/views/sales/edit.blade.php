<div class="modal fade" id="modal-edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDIT SALES</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <input type="hidden" id="sales_id">

                <div class="form-group">
                    <label for="productId" class="control-label">Product</label>
                    <input type="text" class="form-control" id="productId-edit">
                    <div class="text-danger mt-2 d-none" role="alert" id="alert-productId-edit"></div>                      
                </div>
                

                <div class="form-group">
                    <label for="salesPersonId">Price</label>
                    <input type="text" class="form-control" id="salesPersonId-edit">
                    <div class="text-danger mt-2 d-none" role="alert" id="alert-salesPersonId-edit"></div>
                </div>

                <div class="form-group">
                    <label for="salesDate">Date</label>
                    <input type="text" class="form-control" id="salesDate-edit">
                    <div class="text-danger mt-2 d-none" role="alert" id="alert-salesDate-edit"></div>
                </div>

                <div class="form-group">
                    <label for="salesAmmount">Ammount</label>
                    <input type="text" class="form-control" id="salesAmmount-edit">
                    <div class="text-danger mt-2 d-none" role="alert" id="alert-salesAmmount-edit"></div>
                </div>
               

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="update">UPDATE</button>
            </div>
        </div>
    </div>
</div>
  