@extends('adminlte::page')

@section('title', 'Sales')

@section('content_header')
    <h1>Sales</h1>
@stop

@section('content')
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-12">                
                <div class="card">
                    <div class="card-header">
                        <h6> </h6>
                    </div>
                    <div class="card-body">
                        <a href="javascript:void(0)" class="btn btn-success mb-2" id="btn-create-sales">CREATE</a>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Produk Name</th>
                                    <th>Sales Person</th>
                                    <th>Sales Date</th>
                                    <th>Sales Ammount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody id="table-sales">
                                @foreach($salesData as $sales)
                                    <tr id="index_{{ $sales->id }}">
                                        <td>{{ $sales->id }}</td>
                                        <td>{{ $sales->product->name ?? "-"}}</td>
                                        <td>{{ $sales->sales_person->name ?? "-"}}</td>
                                        <td>{{ $sales->sales_date }}</td>
                                        <td>{{ $sales->sales_ammount }}</td>
                                        <td class="text-center">
                                            <a href="javascript:void(0)" id="btn-edit-sales" data-id="{{ $sales->id }}" class="btn btn-primary btn-sm">EDIT</a>
                                            <a href="javascript:void(0)" id="btn-delete-sales" data-id="{{ $sales->id }}" class="btn btn-danger btn-sm">DELETE</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $salesData->links('pagination::bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('sales.create')
    @include('sales.edit')
</body>
@stop

@section('css')
@stop

@section('js')
<script>
    //button create post event
    $('body').on('click', '#btn-create-sales', function () {

        //open modal
        $('#modal-create').modal('show');
    });

    //action create post
    $('#store').click(function(e) {
        e.preventDefault();

        //define variable
        let productId   = $('#productId').val();
        let salesPersonId = $('#salesPersonId').val();
        let salesDate = $('#salesDate').val();
        let salesAmmount = $('#salesAmmount').val();
        let token   = $("meta[name='csrf-token']").attr("content");
        
        //ajax
        $.ajax({

            url: `/sales`,
            type: "POST",
            cache: false,
            data: {
                    "product_id": productId,
                    "sales_person_id": salesPersonId,
                    "sales_date": salesDate,
                    "sales_ammount": salesAmmount,
                    "_token": token
            },
            success:function(response){

                //show success message
                Swal.fire({
                    type: 'success',
                    icon: 'success',
                    title: `${response.message}`,
                    showConfirmButton: false,
                    timer: 3000
                });

                //data post
                let create = `
                    <tr id="index_${response.data.id}">
                        <td>${response.data.id}</td>
                        <td>${response.data.product_id}</td>
                        <td>${response.data.sales_person_id}</td>
                        <td>${response.data.sales_date}</td>
                        <td>${response.data.sales_ammount}</td>
                        <td class="text-center">
                            <a href="javascript:void(0)" id="btn-edit-sales" data-id="${response.data.id}" class="btn btn-primary btn-sm">EDIT</a>
                            <a href="javascript:void(0)" id="btn-delete-sales" data-id="${response.data.id}" class="btn btn-danger btn-sm">DELETE</a>
                        </td>
                    </tr>
                `;
                
                //append to table
                $('#table-sales').prepend(create);
                
                //clear form
                $('#productId').val('');
                $('#salesPersonId').val('');
                $('#salesDate').val('');
                $('#salesAmmount').val('');

                //close modal
                $('#modal-create').modal('hide');
                

            },
            error:function(error){
                
                if(error.responseJSON.product_id[0]) {
                    //show alert
                    $('#alert-productId').removeClass('d-none');
                    $('#alert-productId').addClass('d-block');

                    //add message to alert
                    $('#alert-productId').html(error.responseJSON.product_id[0]);
                } 

                if(error.responseJSON.sales_person_id[0]) {

                    //show alert
                    $('#alert-salesPersonId').removeClass('d-none');
                    $('#alert-salesPersonId').addClass('d-block');

                    //add message to alert
                    $('#alert-salesPersonId').html(error.responseJSON.sales_person_id[0]);
                }

                if(error.responseJSON.sales_date[0]) {

                    //show alert
                    $('#alert-salesDate').removeClass('d-none');
                    $('#alert-salesDate').addClass('d-block');

                    //add message to alert
                    $('#alert-salesDate').html(error.responseJSON.sales_date[0]);
                }

                if(error.responseJSON.sales_ammount[0]) {

                    //show alert
                    $('#alert-salesAmmount').removeClass('d-none');
                    $('#alert-salesAmmount').addClass('d-block');

                    //add message to alert
                    $('#alert-salesAmmount').html(error.responseJSON.sales_ammount[0]);
                }

            }

        });

    });

    $('body').on('click', '#btn-edit-sales', function () {

        let id = $(this).data('id');

        //fetch detail post with ajax
        $.ajax({
            url: `/sales/${id}`,
            type: "GET",
            cache: false,
            success:function(response){

                //fill data to form
                $('#sales_id').val(response.data.id);
                $('#productId-edit').val(response.data.product_id);
                $('#salesPersonId-edit').val(response.data.sales_person_id);
                $('#salesDate-edit').val(response.data.sales_date);
                $('#salesAmmount-edit').val(response.data.sales_ammount);

                //open modal
                $('#modal-edit').modal('show');
            }
        });
    });

    //action update post
    $('#update').click(function(e) {
        e.preventDefault();

        //define variable
        let id = $('#sales_id').val();
        let productId   = $('#productId-edit').val();
        let salesPersonId = $('#salesPersonId-edit').val();
        let salesDate = $('#salesDate-edit').val();
        let salesAmmount = $('#salesAmmount-edit').val();
        let token   = $("meta[name='csrf-token']").attr("content");
        
        //ajax
        $.ajax({

            url: `/sales/${id}`,
            type: "PUT",
            cache: false,
            data: {
                    "product_id": productId,
                    "sales_person_id": salesPersonId,
                    "sales_date": salesDate,
                    "sales_ammount": salesAmmount,
                    "_token": token
            },
            success:function(response){

                //show success message
                Swal.fire({
                    type: 'success',
                    icon: 'success',
                    title: `${response.message}`,
                    showConfirmButton: false,
                    timer: 3000
                });               

                //data post
                let update = `
                    <tr id="index_${response.data.id}">
                        <td>${response.data.id}</td>
                        <td>${response.data.product_id}</td>
                        <td>${response.data.sales_person_id}</td>
                        <td>${response.data.sales_date}</td>
                        <td>${response.data.sales_ammount}</td>
                        <td class="text-center">
                            <a href="javascript:void(0)" id="btn-edit-sales" data-id="${response.data.id}" class="btn btn-primary btn-sm">EDIT</a>
                            <a href="javascript:void(0)" id="btn-delete-sales" data-id="${response.data.id}" class="btn btn-danger btn-sm">DELETE</a>
                        </td>
                    </tr>
                `;
                //append to post data
                $(`#index_${response.data.id}`).replaceWith(update);

                //close modal
                $('#modal-edit').modal('hide');
                

            },
            error:function(error){
                
                if(error.responseJSON.product_id[0]) {
                    //show alert
                    $('#alert-productId-edit').removeClass('d-none');
                    $('#alert-productId-edit').addClass('d-block');

                    //add message to alert
                    $('#alert-productId-edit').html(error.responseJSON.product_id[0]);
                } 

                if(error.responseJSON.sales_person_id[0]) {

                    //show alert
                    $('#alert-salesPersonId-edit').removeClass('d-none');
                    $('#alert-salesPersonId-edit').addClass('d-block');

                    //add message to alert
                    $('#alert-salesPersonId-edit').html(error.responseJSON.sales_person_id[0]);
                }

                if(error.responseJSON.sales_date[0]) {

                    //show alert
                    $('#alert-salesDate-edit').removeClass('d-none');
                    $('#alert-salesDate-edit').addClass('d-block');

                    //add message to alert
                    $('#alert-salesDate-edit').html(error.responseJSON.sales_date[0]);
                }

                if(error.responseJSON.sales_ammount[0]) {

                    //show alert
                    $('#alert-salesAmmount-edit').removeClass('d-none');
                    $('#alert-salesAmmount-edit').addClass('d-block');

                    //add message to alert
                    $('#alert-salesAmmount-edit').html(error.responseJSON.sales_ammount[0]);
                }

            }

        });

    });

    //button create post event
    $('body').on('click', '#btn-delete-sales', function () {

        let id = $(this).data('id');
        let token   = $("meta[name='csrf-token']").attr("content");

        Swal.fire({
            title: 'Are you sure?',
            text: "Delete this data!",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.isConfirmed) {

                console.log('test');

                //fetch to delete data
                $.ajax({

                    url: `sales/${id}`,
                    type: "DELETE",
                    cache: false,
                    data: {
                        "_token": token
                    },
                    success:function(response){ 

                        //show success message
                        Swal.fire({
                            type: 'success',
                            icon: 'success',
                            title: `${response.message}`,
                            showConfirmButton: false,
                            timer: 3000
                        });

                        //remove post on table
                        $(`#index_${id}`).remove();
                    }
                });

                
            }
        })
    });
    
    
    $('.datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
    });
    
    


</script>

@stop

    
