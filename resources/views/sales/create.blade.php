<div class="modal fade" id="modal-create" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Sales</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <label for="productId" class="control-label">Product</label>
                    <input type="text" class="form-control" id="productId">
                    <div class="text-danger mt-2 d-none" role="alert" id="alert-productId"></div>                      
                </div>
                

                <div class="form-group">
                    <label for="salesPersonId">Sales Person</label>
                    <input type="text" class="form-control" id="salesPersonId">
                    <div class="text-danger mt-2 d-none" role="alert" id="alert-salesPersonId"></div>
                </div>

                <div class="form-group">
                    <label for="salesDate">Date</label>
                    <input type="text" class="form-control datetimepicker" id="salesDate">
                    <div class="text-danger mt-2 d-none" role="alert" id="alert-salesDate"></div>
                </div>

                <div class="form-group">
                    <label for="salesAmmount">Ammount</label>
                    <input type="text" class="form-control" id="salesAmmount">
                    <div class="text-danger mt-2 d-none" role="alert" id="alert-salesAmmount"></div>
                </div>
               
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
                <button type="button" class="btn btn-primary" id="store">SAVE</button>
            </div>
        </div>
    </div>
</div>
