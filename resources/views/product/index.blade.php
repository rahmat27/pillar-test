@extends('adminlte::page')

@section('title', 'Product')

@section('content_header')
    <h1>Product</h1>
@stop

@section('content')
<body>    
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-12">               
                <div class="card">                    
                    <div class="card-body">
                        <a href="javascript:void(0)" class="btn btn-success mb-2" id="btn-create-product">CREATE</a>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Description</th>
                                    <th class= "text-center">Action</th>
                                </tr>
                            </thead>

                            <tbody id="table-product">
                                @foreach($products as $product)
                                    <tr  id="index_{{ $product->id }}">
                                        <td>{{ $product->id }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->price }}</td>
                                        <td>{{ $product->description }}</td>
                                        <td class="text-center">
                                            <a href="javascript:void(0)" id="btn-edit-product" data-id="{{ $product->id }}" class="btn btn-primary btn-sm">EDIT</a>
                                            <a href="javascript:void(0)" id="btn-delete-product" data-id="{{ $product->id }}" class="btn btn-danger btn-sm">DELETE</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $products->links('pagination::bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('product.create')
    @include('product.edit')
</body>
@stop

@section('css')
@stop

@section('js')

<script>
    //button create post event
    $('body').on('click', '#btn-create-product', function () {

        //open modal
        $('#modal-create').modal('show');
    });

    //action create post
    $('#store').click(function(e) {
        e.preventDefault();

        //define variable
        let name   = $('#name').val();
        let price = $('#price').val();
        let description = $('#description').val();
        let token   = $("meta[name='csrf-token']").attr("content");
        
        //ajax
        $.ajax({

            url: `/product`,
            type: "POST",
            cache: false,
            data: {
                    "name": name,
                    "price": price,
                    "description": description,
                    "_token": token
            },
            success:function(response){

                //show success message
                Swal.fire({
                    type: 'success',
                    icon: 'success',
                    title: `${response.message}`,
                    showConfirmButton: false,
                    timer: 3000
                });

                //data post
                let create = `
                    <tr id="index_${response.data.id}">
                        <td>${response.data.id}</td>
                        <td>${response.data.name}</td>
                        <td>${response.data.price}</td>
                        <td>${response.data.description}</td>
                        <td class="text-center">
                            <a href="javascript:void(0)" id="btn-edit-product" data-id="${response.data.id}" class="btn btn-primary btn-sm">EDIT</a>
                            <a href="javascript:void(0)" id="btn-delete-product" data-id="${response.data.id}" class="btn btn-danger btn-sm">DELETE</a>
                        </td>
                    </tr>
                `;
                
                //append to table
                $('#table-product').prepend(create);
                
                //clear form
                $('#name').val('');
                $('#price').val('');
                $('#description').val('');

                //close modal
                $('#modal-create').modal('hide');
                

            },
            error:function(error){
                
                if(error.responseJSON.name[0]) {
                        //show alert
                        $('#alert-name').removeClass('d-none');
                        $('#alert-name').addClass('d-block');

                        //add message to alert
                        $('#alert-name').html(error.responseJSON.name[0]);
                    } 

                    if(error.responseJSON.price[0]) {

                        //show alert
                        $('#alert-price').removeClass('d-none');
                        $('#alert-price').addClass('d-block');

                        //add message to alert
                        $('#alert-price').html(error.responseJSON.price[0]);
                    }

                    if(error.responseJSON.description[0]) {

                        //show alert
                        $('#alert-description').removeClass('d-none');
                        $('#alert-description').addClass('d-block');

                        //add message to alert
                        $('#alert-description').html(error.responseJSON.description[0]);
                    }

            }

        });

    });

    $('body').on('click', '#btn-edit-product', function () {

        let id = $(this).data('id');

        //fetch detail post with ajax
        $.ajax({
            url: `/product/${id}`,
            type: "GET",
            cache: false,
            success:function(response){

                //fill data to form
                $('#product_id').val(response.data.id);
                $('#name-edit').val(response.data.name);
                $('#price-edit').val(response.data.price);
                $('#description-edit').val(response.data.description);

                //open modal
                $('#modal-edit').modal('show');
            }
        });
    });

    //action update post
    $('#update').click(function(e) {
        e.preventDefault();

        //define variable
        let product_id = $('#product_id').val();
        let name   = $('#name-edit').val();
        let price = $('#price-edit').val();
        let description = $('#description-edit').val();
        let token   = $("meta[name='csrf-token']").attr("content");
        
        //ajax
        $.ajax({

            url: `/product/${product_id}`,
            type: "PUT",
            cache: false,
            data: {
                "name": name,
                "price": price,
                "description": description,
                "_token": token
            },
            success:function(response){

                //show success message
                Swal.fire({
                    type: 'success',
                    icon: 'success',
                    title: `${response.message}`,
                    showConfirmButton: false,
                    timer: 3000
                });               

                //data post
                let update = `
                    <tr id="index_${response.data.id}">
                        <td>${response.data.id}</td>
                        <td>${response.data.name}</td>
                        <td>${response.data.price}</td>
                        <td>${response.data.description}</td>
                        <td class="text-center">
                            <a href="javascript:void(0)" id="btn-edit-product" data-id="${response.data.id}" class="btn btn-primary btn-sm">EDIT</a>
                            <a href="javascript:void(0)" id="btn-delete-product" data-id="${response.data.id}" class="btn btn-danger btn-sm">DELETE</a>
                        </td>
                    </tr>
                `;
                //append to post data
                $(`#index_${response.data.id}`).replaceWith(update);

                //close modal
                $('#modal-edit').modal('hide');
                

            },
            error:function(error){
                
                if(error.responseJSON.name[0]) {
                    //show alert
                    $('#alert-name-edit').removeClass('d-none');
                    $('#alert-name-edit').addClass('d-block');

                    //add message to alert
                    $('#alert-name-edit').html(error.responseJSON.name[0]);
                } 

                if(error.responseJSON.price[0]) {

                    //show alert
                    $('#alert-price-edit').removeClass('d-none');
                    $('#alert-price-edit').addClass('d-block');

                    //add message to alert
                    $('#alert-price-edit').html(error.responseJSON.price[0]);
                }

                if(error.responseJSON.description[0]) {

                    //show alert
                    $('#alert-description-edit').removeClass('d-none');
                    $('#alert-description-edit').addClass('d-block');

                    //add message to alert
                    $('#alert-description-edit').html(error.responseJSON.description[0]);
                } 

            }

        });

    });

    //button create post event
    $('body').on('click', '#btn-delete-product', function () {

        let product_id = $(this).data('id');
        let token   = $("meta[name='csrf-token']").attr("content");

        Swal.fire({
            title: 'Are you sure?',
            text: "Delete this data!",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.isConfirmed) {

                console.log('test');

                //fetch to delete data
                $.ajax({

                    url: `product/${product_id}`,
                    type: "DELETE",
                    cache: false,
                    data: {
                        "_token": token
                    },
                    success:function(response){ 

                        //show success message
                        Swal.fire({
                            type: 'success',
                            icon: 'success',
                            title: `${response.message}`,
                            showConfirmButton: false,
                            timer: 3000
                        });

                        //remove post on table
                        $(`#index_${product_id}`).remove();
                    }
                });

                
            }
        })
        
    });

</script>
@stop
