@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
<body>    
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-12">               
                <div class="card">                    
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTabs">
                            <li class="nav-item">
                                <a class="nav-link active" id="salesMonthTab" data-toggle="tab" href="#salesMonth">Sales Per Month</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="salesYearTab" data-toggle="tab" href="#salesYear">Sales Per Year</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="salesPersonTab" data-toggle="tab" href="#salesPerson">Sales Per Person</a>
                            </li>
                        </ul>
                        
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="salesMonth">
                                <canvas id="salesChart" width="800" height="400"></canvas>
                            </div>
                            <div class="tab-pane fade" id="salesYear">
                                <canvas id="yearlySalesChart" width="800" height="400"></canvas>
                            </div>
                            <div class="tab-pane fade" id="salesPerson">
                                <canvas id="salesPersonSalesChart" width="800" height="400"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>      
</body>
@stop

@section('js')
<script>
    function fetchSalesData() {
        return fetch('/dashboard/sales-per-month')
            .then(response => response.json())
            .then(data => ({
                labels: data.map(item => item.month),
                datasets: [{
                    label: 'Total Sales',
                    data: data.map(item => item.total_sales),
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1
                }]
            }));
    }

    function fetchYearlySalesData() {
        return fetch('/dashboard/sales-per-year')
            .then(response => response.json())
            .then(data => ({
                labels: data.map(item => item.year),
                datasets: [{
                    label: 'Total Sales',
                    data: data.map(item => item.total_sales),
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1
                }]
            }));
    }

    function fetchSalesPersonSalesData() {
        return fetch('/dashboard/sales-per-person')
            .then(response => response.json())
            .then(data => ({
                labels: data.map(item => item.sales_person_id),
                datasets: [{
                    label: 'Total Sales',
                    data: data.map(item => item.total_sales),
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1
                }]
            }));
    }

    async function drawChart() {
        const salesData = await fetchSalesData();

        const ctx = document.getElementById('salesChart').getContext('2d');
        new Chart(ctx, {
            type: 'line',
            data: salesData,
            options: {
                scales: {
                    x: {
                        title: {
                            display: true,
                            text: 'Month'
                        }
                    },
                    y: {
                        title: {
                            display: true,
                            text: 'Total Sales'
                        }
                    }
                },
                plugins: {
                    tooltip: {
                        callbacks: {
                            label: function(context) {
                                return `Month: ${context.label}, Total Sales: ${context.parsed.y}`;
                            }
                        }
                    }
                }
            }
        });
    }

    async function drawYearlySalesChart() {
        const yearlySalesData = await fetchYearlySalesData();

        const yearlyCtx = document.getElementById('yearlySalesChart').getContext('2d');
        new Chart(yearlyCtx, {
            type: 'bar',
            data: yearlySalesData,
            options: {
                scales: {
                    x: {
                        title: {
                            display: true,
                            text: 'Year'
                        }
                    },
                    y: {
                        title: {
                            display: true,
                            text: 'Total Sales'
                        }
                    }
                }
            }
        });
    }    

    async function drawSalesPersonSalesChart() {
        const salesPersonSalesData = await fetchSalesPersonSalesData();

        const salesPersonSalesCtx = document.getElementById('salesPersonSalesChart').getContext('2d');
        new Chart(salesPersonSalesCtx, {
            type: 'bar',
            data: salesPersonSalesData,
            options: {
                scales: {
                    x: {
                        title: {
                            display: true,
                            text: 'Sales Person ID'
                        }
                    },
                    y: {
                        title: {
                            display: true,
                            text: 'Total Sales'
                        }
                    }
                }
            }
        });
    }

    window.onload = function() {
        drawChart();
        drawYearlySalesChart();
        drawSalesPersonSalesChart();
    };

    $(document).ready(function(){
        $('#myTabs a').on('click', function (e) {
            e.preventDefault();
            $(this).tab('show');
        });
    });

</script>

@stop