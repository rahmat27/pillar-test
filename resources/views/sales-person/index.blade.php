@extends('adminlte::page')

@section('title', 'Sales Person')

@section('content_header')
    <h1>Sales Person</h1>
@stop

@section('content')
    <div class="container" style="margin-top: 50px">
        <div class="row">
            <div class="col-md-12">                     
                <div class="card">
                    <div class="card-header">
                        <h6> </h6>
                    </div>
                    <div class="card-body">
                        <a href="javascript:void(0)" class="btn btn-success mb-2" id="btn-create-sales-person">CREATE</a>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Contact Number</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody id="table-sales-person">
                                @foreach($salesPersons as $salesPerson)
                                    <tr id="index_{{ $salesPerson->id }}">
                                        <td>{{ $salesPerson->id }}</td>
                                        <td>{{ $salesPerson->name }}</td>
                                        <td>{{ $salesPerson->address }}</td>
                                        <td>{{ $salesPerson->contact_number }}</td>
                                        <td class="text-center">
                                            <a href="javascript:void(0)" id="btn-edit-sales-person" data-id="{{ $salesPerson->id }}" class="btn btn-primary btn-sm">EDIT</a>
                                            <a href="javascript:void(0)" id="btn-delete-sales-person" data-id="{{ $salesPerson->id }}" class="btn btn-danger btn-sm">DELETE</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $salesPersons->links('pagination::bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('sales-person.create')
    @include('sales-person.edit')

@stop

@section('css')
@stop

@section('js')
<script>
    //button create post event
    $('body').on('click', '#btn-create-sales-person', function () {

        //open modal
        $('#modal-create').modal('show');
    });

    //action create post
    $('#store').click(function(e) {
        e.preventDefault();

        //define variable
        let name   = $('#name').val();
        let address = $('#address').val();
        let contactNumber = $('#contactNumber').val();
        let token   = $("meta[name='csrf-token']").attr("content");
        
        //ajax
        $.ajax({

            url: `/sales-person`,
            type: "POST",
            cache: false,
            data: {
                    "name": name,
                    "address": address,
                    "contact_number": contactNumber,
                    "_token": token
            },
            success:function(response){

                //show success message
                Swal.fire({
                    type: 'success',
                    icon: 'success',
                    title: `${response.message}`,
                    showConfirmButton: false,
                    timer: 3000
                });

                //data post
                let create = `
                    <tr id="index_${response.data.id}">
                        <td>${response.data.id}</td>
                        <td>${response.data.name}</td>
                        <td>${response.data.address}</td>
                        <td>${response.data.contact_number}</td>
                        <td class="text-center">
                            <a href="javascript:void(0)" id="btn-edit-sales-person" data-id="${response.data.id}" class="btn btn-primary btn-sm">EDIT</a>
                            <a href="javascript:void(0)" id="btn-delete-sales-person" data-id="${response.data.id}" class="btn btn-danger btn-sm">DELETE</a>
                        </td>
                    </tr>
                `;
                
                //append to table
                $('#table-sales-person').prepend(create);
                
                //clear form
                $('#name').val('');
                $('#address').val('');
                $('#contactNumber').val('');

                //close modal
                $('#modal-create').modal('hide');
                

            },
            error:function(error){
                
                if(error.responseJSON.name[0]) {
                        //show alert
                        $('#alert-name').removeClass('d-none');
                        $('#alert-name').addClass('d-block');

                        //add message to alert
                        $('#alert-name').html(error.responseJSON.name[0]);
                    } 

                    if(error.responseJSON.address[0]) {

                        //show alert
                        $('#alert-address').removeClass('d-none');
                        $('#alert-address').addClass('d-block');

                        //add message to alert
                        $('#alert-address').html(error.responseJSON.address[0]);
                    }

                    if(error.responseJSON.contact_number[0]) {

                        //show alert
                        $('#alert-contactNumber').removeClass('d-none');
                        $('#alert-contactNumber').addClass('d-block');

                        //add message to alert
                        $('#alert-contactNumber').html(error.responseJSON.contact_number[0]);
                    }

            }

        });

    });

    $('body').on('click', '#btn-edit-sales-person', function () {

        let id = $(this).data('id');

        //fetch detail post with ajax
        $.ajax({
            url: `/sales-person/${id}`,
            type: "GET",
            cache: false,
            success:function(response){

                //fill data to form
                $('#sales-person-id').val(response.data.id);
                $('#name-edit').val(response.data.name);
                $('#address-edit').val(response.data.address);
                $('#contactNumber-edit').val(response.data.contact_number);

                //open modal
                $('#modal-edit').modal('show');
            }
        });
    });

    //action update post
    $('#update').click(function(e) {
        e.preventDefault();

        //define variable
        let salesPersonId = $('#sales-person-id').val();
        let name   = $('#name-edit').val();
        let address = $('#address-edit').val();
        let contactNumber = $('#contactNumber-edit').val();
        let token   = $("meta[name='csrf-token']").attr("content");
        
        //ajax
        $.ajax({

            url: `/sales-person/${salesPersonId}`,
            type: "PUT",
            cache: false,
            data: {
                "name": name,
                "address": address,
                "contact_number": contactNumber,
                "_token": token
            },
            success:function(response){

                //show success message
                Swal.fire({
                    type: 'success',
                    icon: 'success',
                    title: `${response.message}`,
                    showConfirmButton: false,
                    timer: 3000
                });               

                //data post
                let update = `
                    <tr id="index_${response.data.id}">
                        <td>${response.data.id}</td>
                        <td>${response.data.name}</td>
                        <td>${response.data.address}</td>
                        <td>${response.data.contact_number}</td>
                        <td class="text-center">
                            <a href="javascript:void(0)" id="btn-edit-sales-person" data-id="${response.data.id}" class="btn btn-primary btn-sm">EDIT</a>
                            <a href="javascript:void(0)" id="btn-delete-sales-person" data-id="${response.data.id}" class="btn btn-danger btn-sm">DELETE</a>
                        </td>
                    </tr>
                `;
                //append to post data
                $(`#index_${response.data.id}`).replaceWith(update);

                //close modal
                $('#modal-edit').modal('hide');
                

            },
            error:function(error){
                
                if(error.responseJSON.name[0]) {
                    //show alert
                    $('#alert-name-edit').removeClass('d-none');
                    $('#alert-name-edit').addClass('d-block');

                    //add message to alert
                    $('#alert-name-edit').html(error.responseJSON.name[0]);
                } 

                if(error.responseJSON.address[0]) {

                    //show alert
                    $('#alert-address-edit').removeClass('d-none');
                    $('#alert-address-edit').addClass('d-block');

                    //add message to alert
                    $('#alert-address-edit').html(error.responseJSON.address[0]);
                }

                if(error.responseJSON.contact_number[0]) {

                    //show alert
                    $('#alert-contactNumber-edit').removeClass('d-none');
                    $('#alert-contactNumber-edit').addClass('d-block');

                    //add message to alert
                    $('#alert-contactNumber-edit').html(error.responseJSON.contact_number[0]);
                } 

            }

        });

    });

    //button create post event
    $('body').on('click', '#btn-delete-sales-person', function () {

        let id = $(this).data('id');
        let token   = $("meta[name='csrf-token']").attr("content");

        Swal.fire({
            title: 'Are you sure?',
            text: "Delete this data!",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonText: 'Yes!'
        }).then((result) => {
            if (result.isConfirmed) {

                //fetch to delete data
                $.ajax({

                    url: `sales-person/${id}`,
                    type: "DELETE",
                    cache: false,
                    data: {
                        "_token": token
                    },
                    success:function(response){ 

                        //show success message
                        Swal.fire({
                            type: 'success',
                            icon: 'success',
                            title: `${response.message}`,
                            showConfirmButton: false,
                            timer: 3000
                        });

                        //remove post on table
                        $(`#index_${id}`).remove();
                    }
                });

                
            }
        })
        
    });

</script>
@stop
    