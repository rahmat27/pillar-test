<div class="modal fade" id="modal-edit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDIT Sales Person</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <input type="hidden" id="sales-person-id">

                <div class="form-group">
                    <label for="name" class="control-label">Name</label>
                    <input type="text" class="form-control" id="name-edit">
                    <div class="text-danger mt-2 d-none" role="alert" id="alert-name-edit"></div>
                </div>                

                <div class="form-group">
                    <label class="control-label">Address</label>
                    <textarea class="form-control" id="address-edit" rows="4"></textarea>
                    <div class="text-danger mt-2 d-none" role="alert" id="alert-address-edit"></div>
                </div>
                
                <div class="form-group">
                    <label class="control-label">Contact Number</label>
                    <input type="text" class="form-control" id="contactNumber-edit" rows="4"></textarea>
                    <div class="text-danger mt-2 d-none" role="alert" id="alert-contactNumber-edit"></div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="update">UPDATE</button>
            </div>
        </div>
    </div>
</div>
  