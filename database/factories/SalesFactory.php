<?php


namespace Database\Factories;

use App\Models\Sales;
use Illuminate\Database\Eloquent\Factories\Factory;

class SalesFactory extends Factory
{
    protected $model = Sales::class;

    public function definition()
    {
        return [
            'product_id' => $this->faker->numberBetween(1, 100),
            'sales_person_id' => $this->faker->numberBetween(1, 1000),
            'sales_date' => $this->faker->dateTimeBetween('2010-01-01', '2023-12-31'),
            'sales_ammount' => $this->faker->numberBetween(1000, 100000),
        ];
    }
}