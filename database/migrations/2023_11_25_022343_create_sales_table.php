<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('sales_person_id');
            $table->dateTime('sales_date');
            $table->Biginteger('sales_ammount');
            $table->timestamps();

            $table->index('sales_date', 'idx_sales_date');
            $table->index('product_id', 'idx_product_id');
            $table->index('sales_person_id', 'idx_sales_person_id');
            $table->index(['sales_date', 'sales_ammount'], 'idx_sales_date_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
};
