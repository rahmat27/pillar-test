<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Sales;

class SalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Nonaktifkan indeks dan kunci asing
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::statement('ALTER TABLE sales DISABLE KEYS;');

        // Adjust the batch size based on your system's capacity
        $batchSize = 10000;
        $totalData = 2000000;

        // Using the chunk method to optimize insertion in batches
        DB::table('sales')->truncate();
        $this->command->getOutput()->progressStart($totalData);

        $chunks = $totalData / $batchSize;

        for ($i = 0; $i < $chunks; $i++) {
            DB::beginTransaction();
            try {
                $data = Sales::factory()->count($batchSize)->make()->toArray();
                Sales::insert($data);
                DB::commit();
            } catch (\Exception $e) {
                echo $e;
            }
            $this->command->getOutput()->progressAdvance($batchSize);
        }

        // Aktifkan indeks dan kunci asing kembali
        DB::statement('ALTER TABLE sales ENABLE KEYS;');
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->command->getOutput()->progressFinish();
    }
}


