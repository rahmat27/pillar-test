<?php

namespace Database\Seeders;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DataUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
  
        $name = "admin";
        $email = "admin@mail.co.id";
        $password = Hash::make("admin123");

        $user = User::query()->updateOrcreate(["email" => $email], [
            'name' => $name,
            'email' => $email,
            'password' => $password,
        ]);
        
        $role = Role::create(['name' => 'Admin']);
        
        $user->assignRole([$role->id]);

        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete'        
        ];
      
        foreach ($permissions as $permission) {
              Permission::create(['name' => $permission]);
         }
     
        $permissions = Permission::pluck('id','id')->all();
   
        $role->syncPermissions($permissions);
     
      
       
    }
}
