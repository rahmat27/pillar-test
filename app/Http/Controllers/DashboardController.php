<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Sales;
use Illuminate\Http\Request;

class DashboardController extends Controller
{   

    public function index(){
        return view('dashboard.dashboard');
    }

    public function salesPerMonth()
    {
        $monthlySales = Sales::selectRaw("DATE_FORMAT(sales_date, '%Y-%m') as month, SUM(sales_ammount) as total_sales")
            ->groupBy('month')
            ->orderBy('month')
            ->get();

        return response()->json($monthlySales);
    }

    public function salesPerYear()
    {
        $yearlySales = Sales::selectRaw("YEAR(sales_date) as year, SUM(sales_ammount) as total_sales")
            ->groupBy('year')
            ->orderBy('year')
            ->get();

        return response()->json($yearlySales);
    }

    public function salesPerPerson()
    {
        $salesPersonSales = Sales::selectRaw("sales_person_id, SUM(sales_ammount) as total_sales")
            ->groupBy('sales_person_id')
            ->orderBy('sales_person_id')
            ->get();

        return response()->json($salesPersonSales);
    }
}