<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Mpdf\MpdfException;

class ApiController extends BaseController
{
    public function __construct(Request $request)
    {
        // if (!$request->has("hasbeencreated")) {

        //     $apilog_id = DB::table('api_logs')->insertGetId(["url" => URL::current(), "request" => substr(json_encode($request->all()), 0, 500), "ip" => request()->ip(), "ua" => request()->header('user_agent')]);
        //     $request->merge(["apilog_id" => $apilog_id, "hasbeencreated" => true]);
        // }
    }

    public function sendMail($data, $from, $to, $subject, $view)
    {
        $mail = Mail::send($view, $data, function ($m) use ($data, $from, $to, $subject) {
            $m->from($from["email"], $from["name"]);
            $m->to($to["email"], $to["name"])->subject($subject);
        });
        return $mail;
    }

    static function error_responses($data = [], $message = "Failed")
    {
        if (is_string($data)) {
            $message = $data;
            $data = [];
        } else {
            $tmp = json_decode(json_encode($data), true);
            $message = "Something Trouble";
            if (!empty($tmp)) {
                if (count($tmp) > 0) {
                    foreach ($tmp as $key => $value) {
                        $message = "Error Validation on " . $key . " : " . $value[0];
                        break;
                    }
                }
            }
        }
        return response()->json([
            "status" => false,
            "message" => $message,
            "data" => $data
        ], 404);
    }

    static function error_validation($data = [], $message = "Failed")
    {
        if (is_string($data)) {
            $message = $data;
            $data = [];
        } else {
            $tmp = json_decode(json_encode($data), true);
            $message = "Something Trouble";
            if (!empty($tmp)) {
                if (count($tmp) > 0) {
                    foreach ($tmp as $key => $value) {
                        $message = "Error Validation on " . $key . " : " . $value[0];
                        break;
                    }
                }
            }
        }
        return response()->json([
            "status" => false,
            "message" => $message,
            "data" => $data
        ], 406);
    }

    /**
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    static function success_responses($data = [], $message = 'Success')
    {
        return response()->json([
            "status" => true,
            "message" => $message,
            "data" => json_decode(json_encode($data), true)
        ]);
    }
  
}
