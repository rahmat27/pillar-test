<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\RuleHelper;
use App\Http\Controllers\ApiController;
use App\Models\Sales;

class SalesController extends ApiController
{   
    use RuleHelper;

    public function index(){
        $page_size = 10;

        $salesData = Sales::paginate($page_size);
        
        return view('sales.index', compact(['salesData']));
    }

    public function show($id){

        $salesData = Sales::find($id);

        if($salesData){            
            return static::success_responses($salesData);
        }

    }

    public function create()
	{
		return view('product.create');
	}

    public function store(Request $request){

        if($input_error = $this->check_input_validation()) 
            return response()->json($input_error, 422);

        $data = $request->only('product_id', 'sales_person_id', 'sales_date', 'sales_ammount');

        $sales = Sales::create($data);

        if($sales){
            return static::success_responses($sales, "Success Create Sales");
        }

    }

    public function update(Request $request, $id){

        if($input_error = $this->check_input_validation()) 
            return response()->json($input_error, 422);

        $data = $request->only('product_id', 'sales_person_id', 'sales_date', 'sales_ammount');

        $sales = Sales::find($id);

        $sales->update($data);

        if($sales){
            return static::success_responses($sales, "Success Update Sales");
        }

    }

    public function destroy($id){

        $sales = Sales::find($id);

        if($sales){
            $sales->delete();
            return static::success_responses($sales, "Success Delete Sales");
        }
    }
}
