<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use App\Http\Controllers\ApiController;
use App\Traits\RuleHelper;

class ProductsController extends ApiController
{
    use RuleHelper;

    public function index(){
        $page_size = 10;

        $products = Products::paginate($page_size);

        return view('product.index', compact(['products']));
    }

    public function list(){

        $products = Products::get();

        return response()->json($products);
    }

    public function show($id){
        $product = Products::find($id);
        
        if($product){            
            return static::success_responses($product);
        }

    }

    public function create()
	{
		return view('product.create');
	}

    public function store(Request $request){

        if($input_error = $this->check_input_validation()) 
            return response()->json($input_error, 422);

        $data = $request->only('name', 'price', 'description');
        $product = Products::create($data);

        if($product){
            return static::success_responses($product, "Success Create Product");
        }

    }

    public function update(Request $request, $id){

        if($input_error = $this->check_input_validation()) 
            return response()->json($input_error, 422);

        $data = $request->only('name', 'price', 'description');

        $product = Products::find($id);

        $product->update($data);

        if($product){
            return static::success_responses($product, "Success Update Product");
        }

    }

    public function destroy($id){

        $product = Products::find($id);

        if($product){
            $product->delete();
            return static::success_responses($product, "Success Delete Product");
        }
    }
}
