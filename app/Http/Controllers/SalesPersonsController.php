<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\RuleHelper;
use App\Models\SalesPersons;
use App\Http\Controllers\ApiController;

class SalesPersonsController extends ApiController
{   
    use RuleHelper;

    public function index(){
        $page_size = 10;

        $salesPersons = SalesPersons::paginate($page_size);
        
        return view('sales-person.index', compact(['salesPersons']));
    }

    public function list(){

        $products = SalesPersons::get();

        return response()->json($products);
    }

    public function show($id){

        $salesPerson = SalesPersons::find($id);

        if($salesPerson){            
            return static::success_responses($salesPerson);
        }

    }

    public function create()
	{
		return view('product.create');
	}

    public function store(Request $request){

        if($input_error = $this->check_input_validation()) 
            return response()->json($input_error, 422);

        $data = $request->only('name', 'address', 'contact_number');

        $salesPerson = SalesPersons::create($data);

        if($salesPerson){
            return static::success_responses($salesPerson, "Success Create Sales Person");
        }

    }

    public function update(Request $request, $id){

        if($input_error = $this->check_input_validation()) 
            return response()->json($input_error, 422);

        $data = $request->only('name', 'address', 'contact_number');

        $salesPerson = SalesPersons::find($id);

        $salesPerson->update($data);

        if($salesPerson){
            return static::success_responses($salesPerson, "Success Update Sales Person");
        }

    }

    public function destroy($id){

        $salesPerson = SalesPersons::find($id);

        if($salesPerson){
            $salesPerson->delete();
            return static::success_responses($salesPerson, "Success Delete Sales Person");
        }
    }

}
