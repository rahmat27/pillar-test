<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Traits\RuleHelper;


class LoginController extends ApiController
{
    use RuleHelper;
    public function register(Request $request)
    {
        $data = $request->only('name', 'email', 'password', 'username');
        $validator = Validator::make($data, [
            'username' => 'required|string',
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|max:50'
        ]);

        if ($validator->fails()) {
            return self::error_responses('Validator Fail');
        }

        $user = User::create([
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return static::success_responses($user, "User Created successfully");
    }

    public function login(Request $request)
    {   
        if ($input_error = $this->check_input_validation()) return static::error_validation($input_error);
        
        $credentials = request(['email', 'password']);
       
        try {
            if (!$token = JWTAuth::attempt($credentials)) {

                request()->merge(["name" => request()->email]);
                $credentials = request(['name', 'password']);

                if (!$token = JWTAuth::attempt($credentials)) {
                    return self::error_responses("Login credentials are invalid");
                }
            }
        } catch (JWTException $e) {
            return $credentials;
            return self::error_responses('Could not create token.');
        }
        
        $user = Auth::user();
        $data = [
            "name" => $user->name,
            "email" => $user->email,
            "role" => $user->getRoleNames()[0],
            "permissions" => $user->getPermissionsViaRoles()->pluck("name")
        ];
       
        return $this->respondWithToken($token, $data);

    }

    public function refresh()
    {   
        try{
            $token = JWTAuth::getToken();
            $user = Auth::user();
            return $this->respondWithToken(JWTAuth::refresh($token), $user);
        }
        catch (JWTException $e){
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                $message = 'Token is Invalid';
                return response()->json([
                    "status" => false,
                    "message" => $message,
                    "datas" => []
                ], 401);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){                 
                $message = 'Token is Expired';
                return response()->json([
                    "status" => false,
                    "message" => $message,
                    "datas" => []
                ], 401);
            }else{
                $message ='Authorization Token not found';
                return response()->json([
                    "status" => false,
                    "message" => $message,
                    "datas" => []
                ], 403);
            }
            
        }
    }

    protected function respondWithToken($token, $user)
    {
        return static::success_responses([
            'token' => 'Bearer '.$token,
            'user' => $user,    
        ], "User login successfully");
    }
   

    public function logout(Request $request)
    {
        $validator = Validator::make($request->only('token'), [
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return self::error_responses('Validator Fail');
        }

        try {
            JWTAuth::invalidate($request->token);
            return static::success_responses([], "User has been logged out");
    
        } catch (JWTException $exception) {
            return static::error_responses([], "Sorry, user cannot be logged out");
        }
    }

    public function check_token(Request $request)
    {

        try {
            $valid = JWTAuth::parseToken()->authenticate();
            return static::success_responses($valid, "Token is valid");
        } catch (JWTException $exception) {
            $valid = "false";
            $message = 'Token is Invalid';
                return response()->json([
                    "status" => false,
                    "message" => $message,
                    "datas" => []
                ], 401);
        }
    }

}
