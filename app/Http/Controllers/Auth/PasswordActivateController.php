<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Models\PasswordActivate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Traits\RuleHelper;

class PasswordActivateController extends ApiController
{
    use RuleHelper;

    public function updatePassword(Request $request)
    {
        $email = Crypt::decrypt($request->email);
        $token = $request->token;
        $user = User::query()->where('email', "=", $email)->first();

        if ($input_error = $this->check_input_validation()) return static::error_validation($input_error);

        $passwordActivation = PasswordActivate::query()->where('email', '=', $email)
            ->where('active', '=', 1)
            ->first();
        if (!$passwordActivation) return self::error_responses([], "Token Credentials Was Exipired");
        $checkHashToken = md5($passwordActivation->token) == $token;
        if (!$checkHashToken) return self::error_responses([], "Token Credentials Is Not Valid");
        $datas = [];
        $datas["password"] = password_encrypt($request->password);
        $datas["active"] = 1;
        $update = $user->update($datas);
        if ($update) {
            $passwordActivation = PasswordActivate::query()->where('email', '=', $email)
                ->where('active', '=', 1)->update(["active" => 0]);
            return self::success_responses($user);
        } else {
            return self::error_responses([], "Unkown error");
        }
    }

    public function checkToken(Request $request)
    {
        try{
            $email = Crypt::decrypt($request->email);
            $token = $request->token;
            $getToken = PasswordActivate::query()
                ->where('email', '=', $email)
                ->where('active', '=', 1)
                ->first();
            if (!$getToken)
                return self::error_responses([], "Token Credentials Was Exipired");
            $checkHashToken = md5($getToken->token) == $token;
            if ($checkHashToken)
                return self::success_responses($token);
            else
                return self::error_responses([], "Token Credentials Is Not Valid");
        }
        
        catch(DecryptException $e){
            return self::error_responses("Email Is Not Valid");
        }
    }

    public function sendLink(Request $request)
    {
        if ($input_error = $this->check_input_validation()) return static::error_validation($input_error);
        
        $user = User::query()
            ->whereNull("deleted_at")
            // ->where("active", "=", 0)
            ->where("email", "=", $request->email)
            ->first();           
        if ($user) {
            $response = $user->sendPasswordActivation();
            return self::success_responses($response);
        } else {
            return self::error_responses("Send email failed. Email not found.");
        }
    }
}
