<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\ApiController;
use App\Models\User;
use Illuminate\Http\Request;

class ResetPasswordController extends ApiController
{
    
    public function update(Request $request)
    {
        $auth = $request->get('auth');
        $user = User::query()
            ->whereNull("deleted_at")
            ->where('id',"=",$auth->id)
            ->first();
        if (password_encrypt($request->old_password) == $user->password && $request->new_password === $request->password_confirm){
            $datas["password"] = password_encrypt($request->new_password);
            $res = $user->update($datas);
            if ($res){
                return self::success_responses($res);
            } else {
                return self::error_responses([],"Unkown error");
            }
        } else {
            return self::error_responses([],"Credentials error");
        }
    }

}
