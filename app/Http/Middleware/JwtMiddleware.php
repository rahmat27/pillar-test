<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class JwtMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
            $request->request->add(['user'=>$user]);
            $request->request->add(['creator_id'=>$user->id]);
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                $message = 'Token is Invalid';
                return response()->json([
                    "status" => false,
                    "message" => $message,
                    "datas" => []
                ], 401);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                $message = 'Token is Expired';
                return response()->json([
                    "status" => false,
                    "message" => $message,
                    "datas" => []
                ], 401);
            }else{
                $message ='Authorization Token not found';
                return response()->json([
                    "status" => false,
                    "message" => $message,
                    "datas" => []
                ], 403);
            }
        }
        return $next($request);
    }
}
