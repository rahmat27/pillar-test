<?php

namespace App\Traits;

use App\Rules\IsUniqueOther;
use Illuminate\Support\Facades\Validator;

trait RuleHelper
{
    public function check_input_validation($custom_var = []){
        $controller = get_called_class();
        $method = debug_backtrace()[1]['function'];
        $path = explode('\\', $controller);
        $controller = array_pop($path);
        $datas = request()->all();
        $validator = Validator::make($datas, $this->rules_lists($controller, $method, $custom_var));
        if($validator->fails()) return $validator->errors();
        else return null;
    }

    private function rules_lists($controller, $method, $params = []){
        //================================LoginController=======================
        if ($controller == "LoginController") {
            if ($method == "login") {
                return [
                    'email' => 'required|max:200',
                    'password' => 'required|min:6',
                ];
            } 
        }
        else if ($controller == "ProductsController") {
            if ($method == "store") {
                return [
                    'name' => 'required|max:200||unique:products',
                    'price' => 'required|integer',
                    'description' => 'required',
                ];
            } elseif ($method == "update") {
                return [
                    'name' => 'required|max:200',
                    'price' => 'required|integer',
                    'description' => 'required',
                ];
            }  
        }
        else if ($controller == "SalesController") {
            if ($method == "store") {
                return [
                    'sales_date' => 'required|date|date_format:Y-m-d H:i:s',
                    'sales_ammount' => 'required|integer',
                    'sales_person_id' => 'required|integer',
                    'product_id' => 'required|integer',
                ];
            } elseif ($method == "update") {
                return [
                    'sales_date' => 'required|date|date_format:Y-m-d H:i:s',
                    'sales_ammount' => 'required|integer',
                    'sales_person_id' => 'required|integer',
                    'product_id' => 'required|integer',
                ];
            }  
        }
        else if ($controller == "SalesPersonsController") {
            if ($method == "store") {
                return [
                    'name' => 'required|max:200',
                    'address' => 'required',
                    'contact_number' => 'required',
                ];
            } elseif ($method == "update") {
                return [
                    'name' => 'required|max:200',
                    'address' => 'required',
                    'contact_number' => 'required',
                ];
            }  
        }                  
        return [];
    }
}

?>
